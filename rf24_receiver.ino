#include <SPI.h>
#include "RF24.h"

RF24 radio(9,10);

const uint64_t address = 0xABCDABCD71LL;

void setup() {
  Serial.begin(9600);
  
  radio.begin();
  radio.setAutoAck(0,false);
    
  radio.openReadingPipe(0, address);

  radio.startListening();
}

void loop() {   
    unsigned long got_time;                                 
    while (radio.available()) {
        radio.read( &got_time, sizeof(unsigned long) );
        Serial.println(got_time);  
    }
}     