#include <SPI.h>
#include "RF24.h"

RF24 radio(9,10);

const uint64_t address = 0xABCDABCD71LL;

void setup() {
  Serial.begin(9600);
  
  radio.begin();

  radio.setAutoAck(0,false);
  
  radio.stopListening();         
                         
  radio.openWritingPipe(address);
}

void loop() {
  unsigned long start_time = micros();                          
  
  Serial.println(start_time);
  
  if (!radio.write( &start_time, sizeof(unsigned long) )) {
       Serial.println(F("failed"));
  }
  
  delay(100);
}